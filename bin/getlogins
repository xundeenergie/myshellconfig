#!/usr/bin/bash

# Grab a point in time snapshot of the output from the last command
last > /tmp/last_output

# Alternatively because there are output variances to deal with,
last | grep '+' > /tmp/day_more && last | grep -v '+' > /tmp/day_less

# Now we have complete output of last, and split versions of sessions
# that are shorter or longer than one day. We should convert days in
# the day_more file to hours and append to day_less for consistency.
# placeholder to code this thought process...

# we are not interested in 00 minute entries (unless we also decide to count logins)
# We are not interested in any users that are currently logged in or the wtmp footer
# We are not interested in system entries either. Let's hope no users match our egrep
egrep -v '(00:00|logged|^$|wtmp|system)' /tmp/last_output | sed 's/[()]//g' > /tmp/last_clean
cut -d' ' -f1 /tmp/last_clean | sort -u > /tmp/unique_users

# We can crunch data (output) in several ways; Read a line, test each field, process...
# Or we can break our data into separate outputs (by user) before crunching the data.
for user in $(cat /tmp/unique_users)
do
# Guarantee we are starting each loop with clean variables
minute=0
tm=0
hour=0
th=0
day=0
td=0

for time in $(awk /^${user}/'{print $NF}' /tmp/last_clean)
do
temp=$(echo ${time} | cut -d+ -f1)
if [ ${time} = ${temp} ]; then # The day indicator (+) does not exist
day=0
hour=$(echo ${time} | cut -d: -f1 | sed 's/^0//')
else
day=$(echo ${time} | cut -d+ -f1)
hour=$(echo ${time} | cut -d+ -f2 | cut -d: -f1 | sed 's/^0//')
fi
# We use sed to strip padded 0's in single digit values for math (07 becomes just 7)
minute=$(echo ${time} | cut -d: -f2 | sed 's/^0//')
((td = td + day))
((th = th + hour))
((tm = tm + minute))
done
# We have our column totals, but now we need to normalize the output again
minute=$(expr $tm % 60)
hour=$(expr `echo "$tm / 60 + $th" | bc` % 24)
day=$(expr `echo "$tm / 60 + $th" | bc` / 24 + $td)
printf "%8s %4d + %02d : %02d \n" $user $day $hour $minute
done
